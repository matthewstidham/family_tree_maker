# Inspired by https://medium.com/@ahsenparwez/building-a-family-tree-with-python-and-graphviz-e4afb8367316
from graphviz import Digraph
import networkx as nx
import pandas as pd
import numpy as np
import itertools
class genealogy:
    def __init__(self,
                csvs=None):
        self.ancestry = pd.concat([pd.read_csv(csv).dropna(subset=['Person 1','Gender']) for csv in csvs])
        self.graph=nx.Graph()
        self.graph.add_edges_from(list(self.ancestry[['Person 1','Person 2']].dropna().to_records(index=False)))
        
    def mainfamilytree(self,individuals,ancestry=None):
        if ancestry is None:
            ancestry=self.ancestry
        dot = Digraph(comment = 'Ancestry', graph_attr = {'splines':'polyline'})
        for person in individuals:
            try:
                g=[*ancestry.loc[ancestry['Person 1'] == person]['Gender']][0]
            except:
                print(person)
            sh = 'rect' if g == 'M' else 'ellipse'
            dot.node(person, person, shape = sh)
        # Spouse Relationship
        this_gen = ancestry[ancestry['Relation']=='Spouse']
        if len(this_gen) > 0:
            for j in range(0, len(this_gen)):
                per1 = this_gen['Person 1'].iloc[j]
                per2 = this_gen['Person 2'].iloc[j]
                with dot.subgraph() as subs:
                    subs.attr(rank = 'same')
                    subs.edge(per2, per1, arrowhead = 'none', color = "black:invis:black")

        # Child Relationship
        this_gen = ancestry[ancestry['Relation']=='Child']
        if len(this_gen) > 0:
            for j in range(0, len(this_gen)):
                per1 = this_gen['Person 1'].iloc[j]
                per2 = this_gen['Person 2'].iloc[j]
                per3 = this_gen['Person 3'].iloc[j]
                dot.edge(per2, per1)
                if type(per3) is str:
                    dot.edge(per3, per1)
        dot.format = 'svg'
        return dot
    
    def printfamilytree(self, individuals,dest,ancestry=None):
        dot=self.mainfamilytree(individuals,ancestry)
        dot.render(dest, view=True)
    
    def generatefamilytree(self,dest):
        ancestry=self.ancestry
        individuals=set(list(ancestry['Person 1'])+list(ancestry.dropna(subset=['Person 2'])['Person 2']))
        self.printfamilytree(individuals,dest)
        
    def getancestors(self,person):
        ancestorset = set()
        ancestors = dict()
        ancestors[1]=[person]
        generation=list()
        generation.extend(list(self.ancestry[(self.ancestry['Person 1']==person)&
                                        (self.ancestry['Relation']=='Child')]['Person 2']))
        generation.extend(list(self.ancestry[(self.ancestry['Person 1']==person)&
                                        (self.ancestry['Relation']=='Child')]['Person 3']))
        ancestors[2]=generation
        for person in ancestors[2]:
            ancestorset.add(person)
        stop_function = 0
        generation = 3
        while stop_function == 0:
            print(generation)
            individuals1=ancestors[generation-1]
            individuals2=[]
            for individual in individuals1:
                if individual in ancestorset:
                    parent1=list(self.ancestry[(self.ancestry['Person 1']==individual)&
                                          (self.ancestry['Relation'].isin(['Child','Spouse']))]['Person 2'])
                    parent2=list(self.ancestry[(self.ancestry['Person 1']==individual)&
                                          (self.ancestry['Relation'].isin(['Child','Spouse']))]['Person 3'])
                    for parent in [parent1,parent2]:
                        if parent not in list(ancestorset):
                            if len(parent) > 0:
                                ancestorset.add(parent[0])
                                individuals2.append(parent[0])
            ancestors[generation]=individuals2
            generation +=1
            if set(ancestors[generation-1]) == set(ancestors[generation-2]):
                stop_function = 1
        return ancestors
    
    def generateancestortree(self, dest, person):
        ancestry=self.ancestry
        targets=list(set(list(itertools.chain.from_iterable(list(self.getancestors(person).values())))))
        for individual in targets:
            targets.extend(list(ancestry[(ancestry['Person 2']==individual)&(ancestry['Relation'].isin(['Spouse']))]['Person 1']))
        targets=list(set(targets))
        ancestry = ancestry[ancestry['Person 1'].isin(targets)]
        individuals=set(list(ancestry['Person 1'])+list(ancestry.dropna(subset=['Person 2'])['Person 2']))
        self.printfamilytree(individuals,dest,ancestry)
        
    def returngraph(self):
        ancestry=self.ancestry
        individuals=set(list(ancestry['Person 1'])+list(ancestry.dropna(subset=['Person 2'])['Person 2']))
        return self.mainfamilytree(individuals)

        
     
    def closestrelationship(self, person1, person2):
        G=nx.Graph()
        G.add_edges_from(list(self.ancestry[['Person 1','Person 2']].dropna().to_records(index=False)))
        return nx.bidirectional_shortest_path(G, person1, person2)
    
    def centralnode(self):
        graph=nx.degree_centrality(self.graph)
        return pd.DataFrame(graph.items()).sort_values(1,ascending=False)
    
    def missingancestors(self, person):
        ancestors = list(set(list(itertools.chain.from_iterable(list(self.getancestors(person).values())))))
        missingancestors = list()
        for person in ancestors:
            try:
                parents = self.ancestry[(self.ancestry['Person 1']==person)&
                                        (self.ancestry['Relation'] == 'Child')][['Person 2','Person 3']].values.tolist()[0]
            except:
                parents=[]
            if len(parents) == 0:
                missingancestors.append(person)
        return missingancestors